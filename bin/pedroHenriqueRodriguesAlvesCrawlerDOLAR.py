import pandas as pd
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
import datetime
import re


pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
ts = datetime.datetime.now()
ts_save = f'{ts.year}-{ts.month}-{ts.day}_{ts.hour}:{ts.minute}'

req = Request('https://m.investing.com/currencies/usd-brl', headers={'User-Agent': 'Mozilla/5.0'})
webpage = urlopen(req).read()
soup = BeautifulSoup(webpage, 'html.parser')

find_currency = soup.find_all(class_='instrumentH1inlineblock')
currency_split = str(find_currency).split('\t')
currency = currency_split[4]

find_value = soup.find_all(class_= 'lastInst pid-2103-last')
value_split_n = str(find_value).split('\n')
value_split_t = value_split_n[1].split('<')
value_sub = value_split_t[0]
value = re.sub(' ','',value_sub)

find_change_perc = soup.findAll(class_='quotesChange')
change_perc = str(find_change_perc).split()
change = change_perc[6]
perc = change_perc[15]

find_time = soup.findAll(class_='pid-2103-time')
time_list = re.findall('>(.*)<', str(find_time))
time = ', '.join(time_list)

obj = {'currency': [currency], 'value': [value], 'change': [change], 'perc': [perc], 'time': [time] }
df = pd.DataFrame(data=obj)
df.to_csv(f'/home/semantix/workspace/challenge/crawler_dolar/dolar_{ts_save}.csv', sep=';', header=False, index=False)
