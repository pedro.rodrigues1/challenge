#!/bin/bash

CURRENTDATE=`date +"%Y-%m-%d"`
JSON_IBOV=/home/semantix/workspace/challenge/crawler_ibov/processados
JSON_NASDAQ=/home/semantix/workspace/challenge/crawler_nasdad/processados

python3 /home/semantix/workspace/challenge/bin/pedroHenriqueRodriguesAlvesCrawlerBOVESPA.py
python3 /home/semantix/workspace/challenge/bin/pedroHenriqueRodriguesAlvesCrawlerDOLAR.py
python3 /home/semantix/workspace/challenge/bin/pedroHenriqueRodriguesAlvesCrawlerNASDAQ.py
spark-submit --driver-memory 2G --executor-memory 2G --executor-cores 2 --num-executors 4 /home/semantix/workspace/challenge/bin/conversor.jar


for entry_ibov in "${JSON_IBOV}"/*
do
  echo "$entry_ibov"
done
echo "$entry_ibov"

for entry_nasdaq in "${JSON_NASDAQ}"/*
do
  echo "$entry_nasdaq"
done
echo "$entry_nasdaq"

curl -XPUT "http://localhost:9200/ibov_nasdad-$CURRENTDATE" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "number_of_shards": 1,
    "number_of_replicas": 0
  }
}'

curl -XPUT "http://localhost:9200/ibov_nasdad-$CURRENTDATE/_alias/ibov_nasdad"

curl -XPOST "http://localhost:9200/ibov_nasdad-$CURRENTDATE/_doc/_bulk" -H "Content-Type: application/x-ndjson" --data-binary @${entry_ibov}

curl -XPOST "http://localhost:9200/ibov_nasdad-$CURRENTDATE/_doc/_bulk" -H "Content-Type: application/x-ndjson" --data-binary @${entry_nasdaq}

tar -zcf ${entry_ibov}.tar ${entry_ibov}
mv ${entry_ibov}.tar /home/semantix/workspace/challenge/crawler_ibov/indexados/

tar -zcf ${entry_nasdaq}.tar ${entry_nasdaq}
mv ${entry_nasdaq}.tar /home/semantix/workspace/challenge/crawler_nasdad/indexados/


