import java.text.SimpleDateFormat
import java.util.{Calendar, Date, TimeZone}
import java.io.{BufferedWriter, FileWriter, File}


object Conversor {

  def main(args: Array[String]) {
    println("Convertido")
  }

  def getListOfFiles(dir: String): List[String] = {
    val file = new File(dir)
    file.listFiles.filter(_.isFile).map(_.getPath).toList
  }

    val path_dolar = getListOfFiles("/home/semantix/workspace/challenge/crawler_dolar").sorted.last
    val path_nasdad = getListOfFiles("/home/semantix/workspace/challenge/crawler_nasdad").sorted.last
    val path_ibov = getListOfFiles("/home/semantix/workspace/challenge/crawler_ibov").sorted.last
 
    TimeZone.setDefault(TimeZone.getTimeZone("GMT-03:00"))
    val time_save = new SimpleDateFormat("y-M-d_h:m").format(Calendar.getInstance().getTime())
    val json_nasdaq = new BufferedWriter(new FileWriter(new File("/home/semantix/workspace/challenge/crawler_nasdad/processados/nasdaq_"+time_save+".json"), true))
    val json_ibov = new BufferedWriter(new FileWriter(new File("/home/semantix/workspace/challenge/crawler_ibov/processados/ibov_"+time_save+".json"), true))
    
   // val dolar =  scala.io.Source.fromFile(path_dolar)
    val dolar =  scala.io.Source.fromFile(path_dolar, enc="UTF8")
   // val nasDaq = scala.io.Source.fromFile(path_nasdad)
    val nasDaq =  scala.io.Source.fromFile(path_nasdad, enc="UTF8")
    for (line <- dolar.getLines) {
      val dolarCols = line.split(";").map(_.trim)
      val value_dolar: Float = dolarCols(1).toFloat
    for (line <- nasDaq.getLines) {
      val cols = line.split(";").map(_.trim)
      val last_us: Float = cols(2).toFloat
      var high_us: Float = cols(3).toFloat
      var low_us: Float = cols(4).toFloat
      var last_rs: Float = last_us / value_dolar
      val last_rs_f = last_rs.toString.replace(',', '.')
      var high_rs: Float = high_us / value_dolar
      val high_rs_f = high_rs.toString.replace(',', '.')
      var low_rs: Float = low_us / value_dolar
      val low_rs_f = low_rs.toString.replace(',', '.')
      var chg = cols(5)
      var chg_perc = cols(6)
      var vol = cols(7)
      var time = cols(8)
      var name = cols(1)

      json_nasdaq.write("{\"index\":{\"_id\":\""+name+"\"}}\n")
      json_nasdaq.write("{")
      json_nasdaq.write("\"name\":"+"\""+name+"\",")
      json_nasdaq.write("\"last_us\":"+last_us+",")
      json_nasdaq.write("\"high_us\":"+high_us+",")
      json_nasdaq.write("\"low_us\":"+low_us+",")
      json_nasdaq.write("\"last_rs\":"+last_rs_f+",")
      json_nasdaq.write("\"high_rs\":"+high_rs_f+",")
      json_nasdaq.write("\"low_rs\":"+low_rs_f+",")
      json_nasdaq.write("\"chg\":"+chg+",")
      json_nasdaq.write("\"chg_perc\":"+"\""+chg_perc+"\",")
      json_nasdaq.write("\"vol\":"+"\""+vol+"\",")
      json_nasdaq.write("\"time\":"+"\""+time+"\"")
      json_nasdaq.write("}")
      json_nasdaq.write("\n")
    
    }
    }
  json_nasdaq.write("\n")
  json_nasdaq.close()
  

  val ibov =  scala.io.Source.fromFile(path_ibov, enc="UTF8")
  // val ibov =  scala.io.Source.fromFile(new File(path_ibov), "ISO-8859-1")
   
  for (line <- ibov.getLines()) {
      val cols = line.split(";").map(_.trim)
      val last_rs = cols(2)
      var high_rs = cols(3)
      var low_rs = cols(4)
      var chg = cols(5)
      var chg_perc = cols(6)
      var vol = cols(7)
      var time = cols(8)
      var name = cols(1)

      json_ibov.write("{\"index\":{\"_id\":\""+name+"\"}}\n")
      json_ibov.write("{")
      json_ibov.write("\"name\":"+"\""+name+"\",")
      json_ibov.write("\"last_rs\":"+last_rs+",")
      json_ibov.write("\"high_rs\":"+high_rs+",")
      json_ibov.write("\"low_rs\":"+low_rs+",")
      json_ibov.write("\"chg\":"+chg+",")
      json_ibov.write("\"chg_perc\":"+"\""+chg_perc+"\",")
      json_ibov.write("\"vol\":"+"\""+vol+"\",")
      json_ibov.write("\"time\":"+"\""+time+"\"")
      json_ibov.write("}")
      json_ibov.write("\n")
    }
  json_ibov.write("\n")
  json_ibov.close()

}
