import pandas as pd
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
import datetime


pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
ts = datetime.datetime.now()
ts_save = f'{ts.year}-{ts.month}-{ts.day}_{ts.hour}:{ts.minute}'

req = Request('https://www.investing.com/equities/StocksFilter?index_id=17920', headers={'User-Agent': 'Mozilla/5.0'})
webpage = urlopen(req).read()
soup = BeautifulSoup(webpage, 'html.parser')

table = soup.find(name='table')
table_str = str(table)
df = pd.read_html(table_str)[0]
df_format = df[['Name','Last','High','Low', 'Chg.', 'Chg. %', 'Vol.', 'Time']]
df.to_csv(f'/home/semantix/workspace/challenge/crawler_ibov/ibov_{ts_save}.csv', sep=';', header=False, index=False, encoding="utf-8")
